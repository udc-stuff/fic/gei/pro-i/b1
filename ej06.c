#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979323846

int main() {
	float r;

	printf("Radio: ");
	scanf("%f", &r);	

	printf("La superficie de la esfera de radio %f es %f\n", r, 4*PI * powf(r, 2));

	printf("El volumen de la esfera de radio %f es %f\n", r, 4.0f/3 * PI * powf(r, 3));

	return 0;
}
