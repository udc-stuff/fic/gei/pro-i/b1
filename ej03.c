#include <stdio.h>

int main() {
	float base, height;

	printf("Introduzca la base del triangulo: ");
	scanf("%f", &base);
	printf("Introduzca la altura del triangulo: ");
	scanf("%f", &height);

	printf("La superficie del triangulo de base %.2f y altura %.2f es %.2f\n", base, height, base*height/2);
}
