#include <stdio.h>

int main() {
	char name[20];
	int age;

	printf("Indica tu nombre: ");
	scanf("%s", name);
	printf("Indica tu edad:");
	scanf("%d", &age);

	printf("Hola %s, tu edad es %d.\n", name, age);

	return 0;
}
