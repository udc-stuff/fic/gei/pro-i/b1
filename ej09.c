#include <stdio.h>

int main() {
	char name[20];
	int age, children;
	float salary;

	printf("Introduzca su nombre: ");
	scanf("%s", name);

	printf("Introduzca su edad: ");
	scanf("%d", &age);

	printf("Introduzca su numero de hijos: ");
	scanf("%d", &children);

	printf("Introduzca su sueldo anual (en euros): ");
	scanf("%f", &salary);

	printf("Nombre: %s\nEdad: %d\n", name, age);
	printf("Numero de hijos: %d\n", children);
	printf("Sueldo mensual: %.2f (euros)\n", salary/12);	

	return 0;
}
