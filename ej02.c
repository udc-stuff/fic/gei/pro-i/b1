#include <stdio.h>
#define RETIREMENT 67

int main() {
	char surname[50];
	int age;
	
	printf("Introduzca su apellido: ");
	scanf("%s", surname);
	printf("Introduzca su edad: ");
	scanf("%d", &age);

	printf("Sr/Sra. %s, le quedan %d anos para jubilarse\n", surname, RETIREMENT - age);
}
