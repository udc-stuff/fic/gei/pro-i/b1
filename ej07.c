#include <stdio.h>
#define IVA 1.24f

int main() {
	float item; 

	printf("Precio del producto (sin IVA): ");
	scanf("%f", &item);

	printf("El importe total (IVA incluido) es de %.2f euros\n", item * IVA);

	return 0;
}
