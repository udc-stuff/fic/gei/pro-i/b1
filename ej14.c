#include <stdio.h>

int main() {
	char city[20];
	int min_fahrenheit, max_fahrenheit;
	float min_celsius, max_celsius;

	printf("Introduzca el nombre de su ciudad: ");
	scanf("%s", city);

	printf("Introduzca la temperatura máxima en grados Fahrenheit: ");
	scanf("%d", &max_fahrenheit);

	printf("Introduzca la temperatura mínima en grados Fahrenheit: ");
	scanf("%d", &min_fahrenheit);

	min_celsius = (min_fahrenheit - 32) * 5.0f/9;
	max_celsius = (max_fahrenheit - 32) * 5.0f/9;

	printf("T Max (ºF)\tT Min (ºF)\tT Max (ºC)\tT Min (ºC)\n");
	printf("%2d º F\t\t%2d º F\t\t%.2f º C\t%.2f º C\n", 
	       max_fahrenheit, min_fahrenheit, max_celsius, min_celsius);



	return 0;
}
