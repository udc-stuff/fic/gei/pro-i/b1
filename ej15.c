#include <stdio.h>
#include <math.h>

int main() {
	float t, b;
	int num;
	int side;

	printf("Kilogramos de baldosas en el camion: ");
	scanf("%f", &t);

	printf("Kilogramos que pesa una baldosa: ");
	scanf("%f", &b);

	num = (t / b); // numero total de baldosas
	side = sqrt(num); // lado del cuadrado

	printf("El camion transporta %d baldosas y el lado del cuadrado mas grande que puede formar es usando %d baldosas\n", num, side);

	return 0;
}
