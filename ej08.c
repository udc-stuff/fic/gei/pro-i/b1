#include <stdio.h>

int main() {
	char name[20];
	int age;
	float beer, bus;

	printf("Introduzca su nombre: ");
	scanf("%s", name);

	printf("Introduzca su edad: ");
	scanf("%d", &age);

	printf("Introduzca usted el total de sus gastos semanales en cervezas (en euros): ");
	scanf("%f", &beer);

	printf("Introduzca usted el total de sus gastos semanales en transporte (en euros): ");
	scanf("%f", &bus);

	printf("Nombre: %s\nEdad: %d\n", name, age);
	printf("Gasto semanal en cervezas: %.2f\n", beer);
	printf("Gasto semanal en transporte: %.2f\n", bus);
	printf("\nTotal gastos semanales: %.2f\n",  beer + bus);	

	return 0;
}
