#include <stdio.h>

int main() {
	float l1, l2;

	printf("Lado1: ");
	scanf("%f", &l1);

	printf("Lado2: ");
	scanf("%f", &l2);

	printf("El superficie de lados %.2f y %.2f tiene de perimetro %.2f", l1, l2, l1*l2);	

	return 0;
}
