#include <stdio.h>

int main() {
	int i11, i12, i21, i22;
	int j11, j12, j21, j22;

	printf("Primera matriz\n");
	printf("Valor en la posición (1, 1): ");
	scanf("%d", &i11);
	printf("Valor en la posición (1, 2): ");
        scanf("%d", &i12);
	printf("Valor en la posición (2, 1): ");
        scanf("%d", &i21);
	printf("Valor en la posición (2, 2): ");
        scanf("%d", &i22);

	printf("Segunda matriz\n");
        printf("Valor en la posición (1, 1): ");
        scanf("%d", &j11);
        printf("Valor en la posición (1, 2): ");
        scanf("%d", &j12);
        printf("Valor en la posición (2, 1): ");
        scanf("%d", &j21);
        printf("Valor en la posición (2, 2): ");
        scanf("%d", &j22);

	printf("\nResultado:\n");
	printf("%2d\t%2d\n", i11*j11 + i12*j21, i11*j12 + i12*j22);
	printf("%2d\t%2d\n", i21*j11 + i22*j21, i21*j12 + i22*j22);

	return 0;
}
