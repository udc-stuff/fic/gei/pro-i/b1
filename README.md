En este repositorio os encontraréis ejercicios del boletín 1 que se han realizado en clase de prácticas.

Quizás encontréis alguna pequeña modificación con respecto al enunciado.
Recordar que esas pequeñas modificaciones han sido realizadas para explicar cosas concretas que ocurren al usar el lenguaje C.


# Ej 01

```c
#include <stdio.h>

int main() {
	char name[20];
	int age;

	printf("Indica tu nombre: ");
	scanf("%s", name);
	printf("Indica tu edad:");
	scanf("%d", &age);

	printf("Hola %s, tu edad es %d.\n", name, age);

	return 0;
}

```

# Ej 02

```c
#include <stdio.h>
#define RETIREMENT 67

int main() {
	char surname[50];
	int age;
	
	printf("Introduzca su apellido: ");
	scanf("%s", surname);
	printf("Introduzca su edad: ");
	scanf("%d", &age);

	printf("Sr/Sra. %s, le quedan %d anos para jubilarse\n", surname, RETIREMENT - age);
}
```

# Ej 03

```c
#include <stdio.h>

int main() {
	float base, height;

	printf("Introduzca la base del triangulo: ");
	scanf("%f", &base);
	printf("Introduzca la altura del triangulo: ");
	scanf("%f", &height);

	printf("La superficie del triangulo de base %.2f y altura %.2f es %.2f\n", base, height, base*height/2);
}
```

# Ej 04

```c
#include <stdio.h>

int main() {
	float base, height;

	printf("Introduzca la base del rectangulo: ");
	scanf("%f", &base);
	printf("Introduzca la altura del rectangulo: ");
	scanf("%f", &height);

	printf("El perimetro del rectangulo de base %.2f y altura %.2f es %.2f\n", base, height, (base+height)/2);
}
```



# Ej 05

```c
#include <stdio.h>

int main() {
	float l1, l2;

	printf("Lado1: ");
	scanf("%f", &l1);

	printf("Lado2: ");
	scanf("%f", &l2);

	printf("El superficie de lados %.2f y %.2f tiene de perimetro %.2f", l1, l2, l1*l2);	

	return 0;
}
```



# Ej 06

```c
#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979323846

int main() {
	float r;

	printf("Radio: ");
	scanf("%f", &r);	

	printf("La superficie de la esfera de radio %f es %f\n", r, 4*PI * powf(r, 2));

	printf("El volumen de la esfera de radio %f es %f\n", r, 4.0f/3 * PI * powf(r, 3));

	return 0;
}
```



# Ej 07

```c
#include <stdio.h>
#define IVA 1.24f

int main() {
	float item; 

	printf("Precio del producto (sin IVA): ");
	scanf("%f", &item);

	printf("El importe total (IVA incluido) es de %.2f euros\n", item * IVA);

	return 0;
}
```



# Ej 08

```c
#include <stdio.h>

int main() {
	char name[20];
	int age;
	float beer, bus;

	printf("Introduzca su nombre: ");
	scanf("%s", name);

	printf("Introduzca su edad: ");
	scanf("%d", &age);

	printf("Introduzca usted el total de sus gastos semanales en cervezas (en euros): ");
	scanf("%f", &beer);

	printf("Introduzca usted el total de sus gastos semanales en transporte (en euros): ");
	scanf("%f", &bus);

	printf("Nombre: %s\nEdad: %d\n", name, age);
	printf("Gasto semanal en cervezas: %.2f\n", beer);
	printf("Gasto semanal en transporte: %.2f\n", bus);
	printf("\nTotal gastos semanales: %.2f\n",  beer + bus);	

	return 0;
}
```



# Ej 09

```c
#include <stdio.h>

int main() {
	char name[20];
	int age, children;
	float salary;

	printf("Introduzca su nombre: ");
	scanf("%s", name);

	printf("Introduzca su edad: ");
	scanf("%d", &age);

	printf("Introduzca su numero de hijos: ");
	scanf("%d", &children);

	printf("Introduzca su sueldo anual (en euros): ");
	scanf("%f", &salary);

	printf("Nombre: %s\nEdad: %d\n", name, age);
	printf("Numero de hijos: %d\n", children);
	printf("Sueldo mensual: %.2f (euros)\n", salary/12);	

	return 0;
}
```



# Ej 10

```c
#include <stdio.h>

int main() {
	int i1, i2, i3;
	int j1, j2, j3;

	printf("Coordenadas cartesianas primer vector (separadas por espacios): ");
	scanf("%d %d %d", &i1, &i2, &i3);

	printf("Coordenadas cartesianas segundo vector (separadas por espacios): ");
	scanf("%d %d %d", &j1, &j2, &j3);

	printf("Producto escalar: %d\n", i1*j1 + i2*j2 + i3*j3);

	return 0;
}
```



# Ej 11

```c
#include <stdio.h>

int main() {
	int t, hours, minutes, seconds;

	printf("Tiempo en segundos: ");
	scanf("%d", &t);

	hours = t / 3600;
	minutes = t / 60 - hours * 60;
	seconds = t % 60;

	printf("%d segundos son %dh:%dm:%ds\n", t, hours, minutes, seconds);

	return 0;
}

```



# Ej 12

```c
#include <stdio.h>

int main() {
	printf("1\n");
	printf("2\t3\n");
	printf("4\t5\t6\n");
	printf("7\t8\t9\t10\n");
	printf("11\t12\t13\t14\t15\n");
	printf("16\t17\t18\t19\t20\t21\n");

	return 0;
}
```



# Ej 13

```c
#include <stdio.h>
#define PI 3.141592653589793f

int main() {
	int r1, r2, r3;

	printf("Introduce el primer radio: ");
	scanf("%d", &r1);

	printf("Introduce el segundo radio: ");
	scanf("%d", &r2);

	printf("Introduce el tercer radio: ");
	scanf("%d", &r3);

	printf("RADIO\tPERIMETRO\tAREA\n");
	printf("=====\t=========\t====\n");
	printf("%d\t%.2f\t\t%2f\n", r1, 2 * PI * r1, PI * r1 * r1);
	printf("%d\t%.2f\t\t%2f\n", r1, 2 * PI * r2, PI * r2 * r2);
	printf("%d\t%.2f\t\t%2f\n", r1, 2 * PI * r3, PI * r3 * r3);

	return 0;
}

```



# Ej 14

```c
#include <stdio.h>

int main() {
	char city[20];
	int min_fahrenheit, max_fahrenheit;
	float min_celsius, max_celsius;

	printf("Introduzca el nombre de su ciudad: ");
	scanf("%s", city);

	printf("Introduzca la temperatura máxima en grados Fahrenheit: ");
	scanf("%d", &max_fahrenheit);

	printf("Introduzca la temperatura mínima en grados Fahrenheit: ");
	scanf("%d", &min_fahrenheit);

	min_celsius = (min_fahrenheit - 32) * 5.0f/9;
	max_celsius = (max_fahrenheit - 32) * 5.0f/9;

	printf("T Max (ºF)\tT Min (ºF)\tT Max (ºC)\tT Min (ºC)\n");
	printf("%2d º F\t\t%2d º F\t\t%.2f º C\t%.2f º C\n", 
	       max_fahrenheit, min_fahrenheit, max_celsius, min_celsius);



	return 0;
}

```



# Ej 15

```c
#include <stdio.h>
#include <math.h>

int main() {
	float t, b;
	int num;
	int side;

	printf("Kilogramos de baldosas en el camion: ");
	scanf("%f", &t);

	printf("Kilogramos que pesa una baldosa: ");
	scanf("%f", &b);

	num = (t / b); // numero total de baldosas
	side = sqrt(num); // lado del cuadrado

	printf("El camion transporta %d baldosas y el lado del cuadrado mas grande que puede formar es usando %d baldosas\n", num, side);

	return 0;
}

```



# Ej 16

```c
#include <stdio.h>

int main() {
	int i11, i12, i21, i22;
	int j11, j12, j21, j22;

	printf("Primera matriz");
	printf("Valor en la posición (1, 1): ");
	scanf("%d", &i11);
	printf("Valor en la posición (1, 2): ");
        scanf("%d", &i12);
	printf("Valor en la posición (2, 1): ");
        scanf("%d", &i21);
	printf("Valor en la posición (2, 2): ");
        scanf("%d", &i22);

	printf("Segunda matriz");
        printf("Valor en la posición (1, 1): ");
        scanf("%d", &j11);
        printf("Valor en la posición (1, 2): ");
        scanf("%d", &j12);
        printf("Valor en la posición (2, 1): ");
        scanf("%d", &j21);
        printf("Valor en la posición (2, 2): ");
        scanf("%d", &j22);

	printf("\nResultado:\n");
	printf("%2d\t%2d\n", i11+j11, i12+j12);
	printf("%2d\t%2d\n", i21+j21, i22+j22);

	return 0;
}
```



# Ej 17

```c
#include <stdio.h>

int main() {
	int i11, i12, i21, i22;
	int j11, j12, j21, j22;

	printf("Primera matriz\n");
	printf("Valor en la posición (1, 1): ");
	scanf("%d", &i11);
	printf("Valor en la posición (1, 2): ");
        scanf("%d", &i12);
	printf("Valor en la posición (2, 1): ");
        scanf("%d", &i21);
	printf("Valor en la posición (2, 2): ");
        scanf("%d", &i22);

	printf("Segunda matriz\n");
        printf("Valor en la posición (1, 1): ");
        scanf("%d", &j11);
        printf("Valor en la posición (1, 2): ");
        scanf("%d", &j12);
        printf("Valor en la posición (2, 1): ");
        scanf("%d", &j21);
        printf("Valor en la posición (2, 2): ");
        scanf("%d", &j22);

	printf("\nResultado:\n");
	printf("%2d\t%2d\n", i11*j11 + i12*j21, i11*j12 + i12*j22);
	printf("%2d\t%2d\n", i21*j11 + i22*j21, i21*j12 + i22*j22);

	return 0;
}
```



