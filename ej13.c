#include <stdio.h>
#define PI 3.141592653589793f

int main() {
	int r1, r2, r3;

	printf("Introduce el primer radio: ");
	scanf("%d", &r1);

	printf("Introduce el segundo radio: ");
	scanf("%d", &r2);

	printf("Introduce el tercer radio: ");
	scanf("%d", &r3);

	printf("RADIO\tPERIMETRO\tAREA\n");
	printf("=====\t=========\t====\n");
	printf("%d\t%.2f\t\t%2f\n", r1, 2 * PI * r1, PI * r1 * r1);
	printf("%d\t%.2f\t\t%2f\n", r1, 2 * PI * r2, PI * r2 * r2);
	printf("%d\t%.2f\t\t%2f\n", r1, 2 * PI * r3, PI * r3 * r3);

	return 0;
}
