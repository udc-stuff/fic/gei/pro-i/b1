#include <stdio.h>

int main() {
	float base, height;

	printf("Introduzca la base del rectangulo: ");
	scanf("%f", &base);
	printf("Introduzca la altura del rectangulo: ");
	scanf("%f", &height);

	printf("El perimetro del rectangulo de base %.2f y altura %.2f es %.2f\n", base, height, (base+height)*2);
}
