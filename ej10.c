#include <stdio.h>

int main() {
	int i1, i2, i3;
	int j1, j2, j3;

	printf("Coordenadas cartesianas primer vector (separadas por espacios): ");
	scanf("%d %d %d", &i1, &i2, &i3);

	printf("Coordenadas cartesianas segundo vector (separadas por espacios): ");
	scanf("%d %d %d", &j1, &j2, &j3);

	printf("Producto escalar: %d\n", i1*j1 + i2*j2 + i3*j3);

	return 0;
}
