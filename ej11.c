#include <stdio.h>

int main() {
	int t, hours, minutes, seconds;

	printf("Tiempo en segundos: ");
	scanf("%d", &t);

	hours = t / 3600;
	minutes = t / 60 - hours * 60;
	seconds = t % 60;

	printf("%d segundos son %dh:%dm:%ds\n", t, hours, minutes, seconds);

	return 0;
}
